#include "Geometry.hpp"

bool geometry::intersectionRayPlane(
	glm::vec3 const& rayOrigin, glm::vec3 const& rayNDirection,
	glm::vec3 const& minPoint, glm::vec3 const& maxPoint,
	float& distance)
{
	float t = (minPoint.x - rayOrigin.x) / rayNDirection.x;

	if (t > 0)
	{
		glm::vec3 intersection_point(
			rayOrigin.x + t * rayNDirection.x,
			rayOrigin.y + t * rayNDirection.y,
			rayOrigin.z + t * rayNDirection.z);

		if (geometry::between(minPoint.y, maxPoint.y, intersection_point.y))
		{
			if (geometry::between(minPoint.z, maxPoint.z, intersection_point.z))
			{
				distance = t;
				return true;
			}
		}
	}

	return false;
}


bool geometry::intersectionRayPlaneGLM(
	glm::vec3 const& rayOrigin,
	glm::vec3 const& rayNDirection,
	glm::vec3 const& originPoint,
	glm::vec3 const& secondPoint,
	glm::vec3 const& thirdPoint,
	float& distance)
{
	//Parameterform erstellen
	//x = p + s*u + t*v
	glm::vec3 u = glm::normalize(secondPoint - originPoint);
	glm::vec3 v = glm::normalize(thirdPoint - originPoint);

	//Normale auf der Ebene
	glm::vec3 normal = glm::normalize(glm::cross(u, v));


	//Schnittpunkt zur Ebene
	float tmp_distance;

	if (glm::intersectRayPlane(rayOrigin, rayNDirection, originPoint, normal, tmp_distance))
	{
		glm::vec3 intersection_point(
			rayOrigin.x + tmp_distance * rayNDirection.x,
			rayOrigin.y + tmp_distance * rayNDirection.y,
			rayOrigin.z + tmp_distance * rayNDirection.z);

		if (geometry::between(secondPoint, thirdPoint, intersection_point))
		{
			distance = tmp_distance;
			return true;
		}
	}


	return false;
}


bool geometry::between(float const& a, float const& b, float const& x)
{
	if (a < b)
	{
		if (x > a && x < b)
		{
			return true;
		}
	}
	else if (a > b)
	{
		if (x > b && x < a)
		{
			return true;
		}
	}
	else if (a == b && x == a)
	{
		return true;
	}
	return false;
}

bool geometry::between(glm::vec3 const& a, glm::vec3 const& b, glm::vec3 const& x)
{
	if (geometry::between(a.x, b.x, x.x))
	{
		if (geometry::between(a.y, b.y, x.y))
		{
			if (geometry::between(a.z, b.z, x.z))
			{
				return true;
			}
		}
	}
}

bool geometry::equal(float const& a, float const& b)
{
	float eps = 0.001f;

	if (a - b < eps && b - a < eps)
	{
		return true;
	}
	//if (b - a < eps)
	//{
	//	return true;
	//}
	return false;
}