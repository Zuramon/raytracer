//Struct Material
#include <string>

#include "color.hpp"

struct Material
{
	std::string name;
	Color kd;
	Color ka;
	Color ks;
	float m;

	Material(): name(), kd(glm::vec3(0,0,0)), ka(glm::vec3(0,0,0)), ks(glm::vec3(0,0,0)), m(0) {}

	friend std::ostream& operator<<(std::ostream& os, Material const& m)
	{
		os << "Material: " << m.name << ": " << std::endl
			<< "ka: " << m.ka  
			<< "kd: " << m.kd  
			<< "ks: " << m.ks
			<< "m: " << m.m << std::endl;
		return os;
	}
};