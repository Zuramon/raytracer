// -----------------------------------------------------------------------------
// Copyright  : (C) 2014 Andreas-C. Bernstein
// License    : MIT (see the file LICENSE)
// Maintainer : Andreas-C. Bernstein <andreas.bernstein@uni-weimar.de>
// Stability  : experimental
//
// Renderer
// -----------------------------------------------------------------------------

#include "renderer.hpp"

Renderer::Renderer(unsigned w, unsigned h, std::string const& file, Scene s)
  : width_(w)
  , height_(h)
  , colorbuffer_(w*h, Color(0.0, 0.0, 0.0))
  , filename_(file)
  , ppm_(width_, height_)
  , scene_(s)
{}

void Renderer::render()
{
	const std::size_t checkersize = 20;
  
	float distance_to_screen = (height_ / 2) / tan(scene_.get_Camera().fov_x);

	//camera transformation matrix
	glm::vec3 u = glm::normalize(glm::cross(scene_.get_Camera().direction, scene_.get_Camera().up));
	glm::vec3 v = glm::normalize(glm::cross(u, scene_.get_Camera().direction));
	glm::mat4 C = glm::mat4(
		u.x, v.x, scene_.get_Camera().direction.x, scene_.get_Camera().position.x,
		u.y, v.y, scene_.get_Camera().direction.y, scene_.get_Camera().position.y,
		u.z, v.z, scene_.get_Camera().direction.z, scene_.get_Camera().position.z,
		0.0, 0.0, 0.0, 1.0);




	for (unsigned y = 0; y < height_; ++y) 
	{
		for (unsigned x = 0; x < width_; ++x) 
		{
			Pixel p(x, y);

			Color black(0.0, 0.0, 0.0);
			Color white(1.0, 1.0, 1.0);
			p.color = black;

			int ray_dir_x = (x - (width_ / 2));
			int ray_dir_y = (y - (height_ / 2));

			Ray r(glm::vec3(0.0, 0.0, 0.0), glm::vec3(ray_dir_x, ray_dir_y, -distance_to_screen));

			//r *= C;

			Shape *hitted_obj = nullptr;
			
			//p.color += this->ambient(r);
			float distance = INFINITY;
			glm::vec3 intersection_point;
			glm::vec3 normal_at_intersection_point;

			//ambient
			for (auto it : scene_.get_AllShapes())
			{
				float tmp_distance;
				glm::vec3 tmp_intersection;
				glm::vec3 tmp_normal;

				if (it->intersect(r, tmp_distance, tmp_intersection, tmp_normal))
				{
					if (tmp_distance < distance)
					{
						distance = tmp_distance;
						intersection_point = tmp_intersection;
						normal_at_intersection_point = tmp_normal;

						for (auto it_light : scene_.get_Lights())
						{
							if (it_light.light_type == 0)
							{
								p.color += it->get_material()->ka * it_light.la;
							}
						}
						hitted_obj = it;
						
					}
				}
			}

			////diffuse
			if (hitted_obj != nullptr)
			{
				//glm::vec3 intersect_point = r.origin + (distance * glm::normalize(r.direction));
				//glm::vec3 intersection_normal = hitted_obj->get_normal_at(intersect_point);
				
				bool is_in_shadow = false;

				for (auto it : scene_.get_Lights())
				{
					if (it.light_type == 1)
					{
						glm::vec3 vec_to_light = intersection_point - it.position;

						float dot_l_n = glm::dot(glm::normalize(normal_at_intersection_point), glm::normalize(vec_to_light));
						//std::cout << dot_l_n << std::endl;

						if (dot_l_n > 0)
						{
							Ray to_light(it.position, -vec_to_light);

							for (auto it_obj : scene_.get_AllShapes())
							{
								
								
								float d;
								if (it_obj->intersect(to_light, d, glm::vec3(), glm::vec3()))
								{
									is_in_shadow = true;
									//std::cout << it.name << " " << it_obj->get_name() << std::endl;
									//std::cout << "is in shadow" << std::endl;
								}
							}
							if (!is_in_shadow)
							{
								p.color += it.ld * hitted_obj->get_material()->kd * dot_l_n;
								//p.color = glm::normalize(normal_at_intersection_point);



								//specular
								//glm::vec3 r = glm::normalize(vec_to_light) * 2.0f * dot_l_n * glm::normalize(normal_at_intersection_point)/* - vec_to_light*/;
								//glm::vec3 r = (2.0f * dot_l_n * glm::normalize(normal_at_intersection_point) - vec_to_light);
								glm::vec3 r = 2.0f * dot_l_n * glm::normalize(normal_at_intersection_point) - glm::normalize(vec_to_light);
								p.color += it.ld * hitted_obj->get_material()->ks * pow(glm::dot(r, glm::normalize(vec_to_light)), hitted_obj->get_material()->m);

							}						
						}
					}
				}
			}



			//specular



			if (hitted_obj == nullptr)
			{
				p.color = white;
			}
			//else
			//{
			//	//std::cout << p.color << std::endl;
			//}



			write(p);
		}
	}
	ppm_.save(filename_);
}

void Renderer::write(Pixel const& p)
{
	// flip pixels, because of opengl glDrawPixels
	size_t buf_pos = (width_*p.y + p.x);
	if (buf_pos >= colorbuffer_.size() || (int)buf_pos < 0) {
	std::cerr << "Fatal Error Renderer::write(Pixel p) : "
		<< "pixel out of ppm_ : "
		<< (int)p.x << "," << (int)p.y
		<< std::endl;
	} else {
	colorbuffer_[buf_pos] = p.color;
	}

	ppm_.write(p);
}

//Color Renderer::ambient(Ray const& ray)
//{
//	float distance = INFINITY;
//
//	for (auto it : scene_.get_AllShapes())
//	{
//		float tmp_distance = 0.0;
//		if (it->intersect(ray, tmp_distance))
//		{
//			//std::cout << "Treffer! " << it->get_name() << std::endl;
//			if (tmp_distance < distance)
//			{
//				distance = tmp_distance;
//				for (auto it_light : scene_.get_Lights())
//				{
//					if (it_light.light_type == 0)
//					{
//						return it->get_material()->ka * it_light.la;
//					}
//				}
//
//			}
//		}
//	}
//}


