//Geometry Functions
#include <glm\glm.hpp>
#include <glm\gtx\intersect.hpp>

namespace geometry
{
	bool intersectionRayPlane(
		glm::vec3 const& rayOrigin, glm::vec3 const& rayNDirection,
		glm::vec3 const& minPoint, glm::vec3 const& maxPoint,
		float& distance);

	bool intersectionRayPlaneGLM(
		glm::vec3 const& rayOrigin, 
		glm::vec3 const& rayNDirection,
		glm::vec3 const& originPoint, 
		glm::vec3 const& secondPoint,
		glm::vec3 const& thirdPoint,
		float& distance);

	bool between(float const& a, float const& b, float const& x);

	bool between(glm::vec3 const& a, glm::vec3 const& b, glm::vec3 const& x);

	bool equal(float const& a, float const& b);
}