//Struct Kamera

#ifndef BUW_CAMERA_HPP
#define BUW_CAMERA_HPP

#include <string>
#include <glm/vec3.hpp>

struct Camera
{
	std::string name;
	glm::vec3 position; //= glm::vec3(0.0, 0.0, 0.0);
	float fov_x;

	glm::vec3 direction;
	glm::vec3 up;

	friend std::ostream& operator<<(std::ostream& os, Camera const& cam)
	{
		os << "Kamera: " << cam.name << std::endl
			<< "Position: (" << cam.position.x << ", " << cam.position.y << ", " << cam.position.z << ")" << std::endl
			<< "Oeffnungswinkel: " << cam.fov_x << "�" << std::endl;

		return os;
	}

};

#endif //#define BUW_CAMERA_HPP