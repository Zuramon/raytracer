#ifndef BUW_TRIANGLE_HPP
#define BUW_TRIANGLE_HPP

#include "shape.hpp"

class Triangle: public Shape{

public:

	Triangle();

	Triangle(std::string const& name, glm::vec3 const& point1, glm::vec3 const& point2, glm::vec3 const& point3, std::shared_ptr<Material> const& material);

	~Triangle();

	glm::vec3 get_p1() const;

	glm::vec3 get_p2() const;

	glm::vec3 get_p3() const;

	double area() const override;

	double volume() const override;

	std::ostream& print(std::ostream& os) const;

	bool intersect(Ray const& ray, float& distance, glm::vec3& intersection_point, glm::vec3& normal_at_intersection) override;

	void translate(glm::vec3 const& offset) override;

	void rotate(float const& angle, glm::vec3 const& vector) override;

	void scale(float const& value) override;


private:

	glm::vec3 p1_;
	glm::vec3 p2_;
	glm::vec3 p3_;
	
};

#endif //#define BUW_TRIANGLE_HPP
