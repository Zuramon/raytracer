#include "Scene.hpp"

#include <iterator>


Scene::Scene():
lights_(),
materials_(),
//boxes_(),
//spheres_(),
camera_(),
composites_()
{}

Scene::~Scene()
{}


void Scene::add_Light(Light new_Light)
{
	lights_.insert(lights_.end(), new_Light);
}

void Scene::add_Material(Material new_Material)
{
	materials_.insert(materials_.end(), new_Material);
}

void Scene::add_Box(Box* new_Box)
{
	//boxes_.insert(boxes_.end(), new_Box);
	shapes_.push_back(new_Box);
}

void Scene::add_Sphere(Sphere* new_Sphere)
{
	//spheres_.insert(spheres_.end(), new_Sphere);
	shapes_.push_back(new_Sphere);
}

void Scene::add_Composites(Composite new_composite)
{
	composites_.push_back(new_composite);
}

void Scene::set_Camera(Camera new_Camera)
{
	camera_ = new_Camera;
}

void Scene::set_Renderer(Renderer_Info new_render_info)
{
	renderer_ = new_render_info;
}



std::vector<Light> Scene::get_Lights()
{
	return lights_;
}

std::vector<Material> Scene::get_AllMaterials()
{
	return materials_;
}

//std::vector<Box> Scene::get_AllBoxes()
//{
//	return boxes_;
//}
//
//std::vector<Sphere> Scene::get_AllSpheres()
//{
//	return spheres_;
//}

std::vector<Shape*> Scene::get_AllShapes()
{
	return shapes_;
}


Composite Scene::get_Composite(std::string composite_name)
{
	for (std::vector<Composite>::iterator it = composites_.begin(); it != composites_.end(); ++it)
	{
		if (it->composite_name_ == composite_name)
		{
			return *it;
		}
	}	
}

bool Scene::is_Composite(std::string composite_name)
{
	for (std::vector<Composite>::iterator it = composites_.begin(); it != composites_.end(); ++it)
	{
		if ( it->composite_name_ == composite_name)
		{
			return true;
		}
	}
	return false;
}

Camera Scene::get_Camera()
{
	return camera_;
}

bool Scene::get_Material(std::string material_name, std::shared_ptr<Material>& material_ptr)
{
	for (std::vector<Material>::iterator it = materials_.begin(); it != materials_.end(); ++it)
	{
		if (it->name == material_name)
		{
			material_ptr = std::make_shared<Material>(*it);
			return true;
		}
	}
	return false;
}

//bool Scene::get_Box(std::string box_name, std::shared_ptr<Box> box_ptr)
//{
//	for (std::vector<Box>::iterator it = boxes_.begin(); it != boxes_.end(); ++it)
//	{
//		if (it->get_name() == box_name)
//		{
//			box_ptr = std::make_shared<Box>(*it);
//			return true;
//		}
//	}
//	return false;
//}
//
//bool Scene::get_Sphere(std::string sphere_name, std::shared_ptr<Sphere> sphere_ptr)
//{
//	for (std::vector<Sphere>::iterator it = spheres_.begin(); it != spheres_.end(); ++it)
//	{
//		if (it->get_name() == sphere_name)
//		{
//			sphere_ptr = std::make_shared<Sphere>(*it);
//			return true;
//		}
//	}
//	return false;
//}

bool Scene::get_Shape(std::string shape_name, std::shared_ptr<Shape*>& shape_ptr)
{
	for (auto it : shapes_)
	{
		if ( it->get_name() == shape_name)
		{
			shape_ptr = std::make_shared<Shape*>(it);
			return true;
		}
	}
	return false;

}

Renderer_Info Scene::get_renderer()
{
	return renderer_;
}

