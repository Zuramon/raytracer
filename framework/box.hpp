#ifndef BUW_BOX_HPP
#define BUW_BOX_HPP

#include "shape.hpp"

class Box: public Shape{

public:

	Box();

	Box(std::string const& name, glm::vec3 const& min, glm::vec3 const& max, std::shared_ptr<Material> const& material);

	~Box();

	glm::vec3 get_min() const;

	glm::vec3 get_max() const;

	double area() const override;

	double volume() const override;

	std::ostream& print(std::ostream& os) const;

	bool intersect(Ray const& ray, float& distance, glm::vec3& intersection_point, glm::vec3& normal_at_intersection) override;

	void translate(glm::vec3 const& offset) override;

	void rotate(float const& angle, glm::vec3 const& vector) override;

	void scale(float const& value) override;

	glm::vec3 get_normal_at(glm::vec4& point) override;


private:

	glm::vec3 minimum_;
	glm::vec3 maximum_;
	
};

inline std::ostream& operator<<(std::ostream& os, Box const& b)
{
	b.print(os);
	return os;
}

#endif //#define BUW_BOX_HPP