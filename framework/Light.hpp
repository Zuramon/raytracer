//Struct Lichtquelle

#ifndef BUW_LIGHT_HPP
#define BUW_LIGHT_HPP

#include <string>
#include "glm/vec3.hpp"
#include "color.hpp"

struct Light
{
	std::string name;
	int light_type; //defines ambient light 0, diffuse light 1
	glm::vec3 position;
	Color la;
	Color ld;

	Light(): name(), light_type(0), position(glm::vec3(0,0,0)), la(glm::vec3(0,0,0)), ld(glm::vec3(0,0,0)) {}

	friend std::ostream& operator<<(std::ostream& os, Light const& l)
	{
		os << "Licht: " << l.name <<std::endl
			<< "Position: (" << l.position.x << ", " << l.position.y << ", " << l.position.z << ")" <<std::endl
			<< "la: " << l.la 
			<< "ld: " << l.ld << std::endl;

		return os;
	}
};

#endif //#define BUW_LIGHT_HPP