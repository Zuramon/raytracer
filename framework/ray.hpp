#ifndef BUW_RAY_HPP
#define BUW_RAY_HPP

#include "glm/glm.hpp"

struct Ray {

	Ray(glm::vec3 orig, glm::vec3 dir) : origin(orig), direction(dir) {}

	glm::vec3 origin;
	glm::vec3 direction;

	Ray& operator*=(glm::mat4 const& m)
	{
		glm::vec4 r_origin(origin, 1.0);
		glm::vec4 r_direction(direction, 0.0);

		r_origin = r_origin * m;
		r_direction = r_direction * m;

		origin = glm::vec3(r_origin.x, r_origin.y, r_origin.z);
		direction = glm::vec3(r_direction.x, r_direction.y, r_direction.z);

		return *this;
	}

	
	friend Ray operator*(Ray r, glm::mat4 const& m)
	{
		auto tmp(r);
		r *= m;
		return tmp;
	}

};

#endif //#define BUW_RAY_HPP
