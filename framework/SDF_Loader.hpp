//Klasse SDF-Loader
#include "Scene.hpp"

#include <fstream>


class SDF_Loader
{
public:
	SDF_Loader();

	~SDF_Loader();

	bool load_Scene(Scene& new_scene, std::string file);

private:

	std::ifstream filestream_;

};