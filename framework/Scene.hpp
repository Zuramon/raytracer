//Klasse beschreibt und enth�lt alle Elemente in der Scene

#ifndef BUW_SCENE_HPP
#define BUW_SCENE_HPP

//#include "shape.hpp"
#include "box.hpp"
#include "sphere.hpp"
#include "Camera.hpp"
#include "Light.hpp"

#include <map>
#include <vector>
//#include <memory>
//#include <string>

struct Composite
{
	std::string composite_name_;
	//std::vector<std::shared_ptr<Box>> composite_boxes_;
	//std::vector<std::shared_ptr<Sphere>> composite_spheres_;
	std::vector<std::shared_ptr<Shape*>> composite_shapes_;

	friend std::ostream& operator<<(std::ostream& os, Composite const& c)
	{
		os << "Composite: " << c.composite_name_ << std::endl
		//	<< "included Boxes: ";
		//
		//for (std::vector<std::shared_ptr<Box>>::const_iterator it = c.composite_boxes_.begin(); it != c.composite_boxes_.end(); ++it)
		//{
		//	os << it->get()->get_name() << " ";
		//}

		//os << std::endl
		//	<< "included Spheres: ";

		//for (std::vector<std::shared_ptr<Sphere>>::const_iterator it = c.composite_spheres_.begin(); it != c.composite_spheres_.end(); ++it)
		//{
		//	os << it->get()->get_name() << " ";
		//}


		<< "included Objects: ";

		for (auto& it : c.composite_shapes_)
		{
			os << (*it)->get_name() << ", ";
		}

		os << std::endl;

		return os;
	}

};


struct Renderer_Info
{
	unsigned int width;
	unsigned int heigth;
	std::shared_ptr<Camera> camera;
	std::string filename;
};






class Scene
{
public:
	
	Scene();
	~Scene();

	void add_Light(Light new_Light);

	void add_Material(Material new_Material);

	void add_Box(Box* new_Box);

	void add_Sphere(Sphere* new_Sphere);

	void add_Composites(Composite new_composite);

	void set_Camera(Camera new_Camera);

	void set_Renderer(Renderer_Info new_render_info);


	std::vector<Light> get_Lights();

	std::vector<Material> get_AllMaterials();

	//std::vector<Box> get_AllBoxes();

	//std::vector<Sphere> get_AllSpheres();

	std::vector<Shape*> get_AllShapes();

	Composite get_Composite(std::string composite_name);

	bool is_Composite(std::string composite_name);

	Camera get_Camera();

	bool get_Material(std::string material_name, std::shared_ptr<Material>& material_ptr);

	//bool get_Box(std::string box_name, std::shared_ptr<Box> box_ptr);

	//bool get_Sphere(std::string sphere_name, std::shared_ptr<Sphere> sphere_ptr);

	bool get_Shape(std::string shape_name, std::shared_ptr<Shape*>& shape_ptr);

	Renderer_Info get_renderer();



private:

	std::vector<Light> lights_;
	std::vector<Material> materials_;

	//std::vector<Box> boxes_;
	//std::vector<Sphere> spheres_;

	std::vector<Composite> composites_;

	Camera camera_;
	Renderer_Info renderer_;

	//
	std::vector<Shape*> shapes_;

};



#endif
