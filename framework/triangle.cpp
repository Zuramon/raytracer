#include "triangle.hpp"


Triangle::Triangle():
	p1_(glm::vec3(0.0,0.0,0.0)),
	p2_(glm::vec3(0.0,0.0,0.0)),
	p3_(glm::vec3(0.0,0.0,0.0)),
	Shape("new_triangle") 
{

}

Triangle::Triangle(std::string const& name, glm::vec3 const& point1, glm::vec3 const& point2, glm::vec3 const& point3, std::shared_ptr<Material> const& material) :
p1_(point1),
p2_(point2),
p3_(point3),
Shape(name, material)
{}

Triangle::~Triangle()
{
	//std::cout << "Destruktor von Box" << std::endl;
}

glm::vec3 Triangle::get_p1() const {
	return p1_;
}

glm::vec3 Triangle::get_p2() const {
	return p2_;
}

glm::vec3 Triangle::get_p3() const {
	return p3_;
}

double Triangle::area() const 
{
	double ux = p2_.x - p1_.x;
	double uy = p2_.y - p1_.y;
	double uz = p2_.z - p1_.z;
	double vx = p3_.x - p1_.x;
	double vy = p3_.y - p1_.y;
	double vz = p3_.z - p1_.z;

	glm::vec3 AB(ux, uy, uz);
	glm::vec3 AC(vx, vy, vz);

	glm::vec3 cross = glm::cross(AB, AC);

	return 1/2 * glm::length(cross);	
}

std::ostream& Triangle::print(std::ostream& os) const {
	os << "Triangle: " << std::endl
		<< "p1_:"<< p1_.x << " " << p1_.y << " " << p1_.z
		<< " p2_:"<< p2_.x << " " << p2_.y << " " << p2_.z
		<< " p3_:"<< p3_.x << " " << p3_.y << " " << p3_.z  << " ";
	Shape::print(os);

	return os;
}

bool Triangle::intersect(Ray const& ray, float& distance, glm::vec3& intersection_point, glm::vec3& normal_at_intersection) {
	
	//auto result = glm::intersectRayTriangle(
	//	ray.origin, 
	//	ray.normDir,
	//	p1_,
	//	p2_,
	//	p3_,
	//	baryPosition);
	//return result;
	return false;
}

void Triangle::translate(glm::vec3 const& offset)
{
	p1_ += offset;
	p2_ += offset;
	p3_ += offset;
}

void Triangle::rotate(float const& angle, glm::vec3 const& vector)
{
	float radiant = angle * M_PI / 180;

	p1_ = glm::rotate(p1_, radiant, vector);
	p2_ = glm::rotate(p2_, radiant, vector);
	p3_ = glm::rotate(p3_, radiant, vector);
}

void Triangle::scale(float const& value)
{
	p2_ *= value;
	p3_ *= value;
}
