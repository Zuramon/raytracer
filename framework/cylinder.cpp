#include "cylinder.hpp"
#include <math.h>


Cylinder::Cylinder():
	position_(glm::vec3(0.0,0.0,0.0)),
	radius_(0),
	height_(0),
	Shape("new_cylinder") 
{

}

Cylinder::Cylinder(std::string const& name, glm::vec3 const& pos, double const& radius, double const& height, std::shared_ptr<Material> const& material) :
position_(pos),
radius_(radius),
height_(height),
Shape(name, material)
{}

Cylinder::~Cylinder()
{
	//std::cout << "Destruktor von Box" << std::endl;
}

glm::vec3 Cylinder::get_position() const {
	return position_;
}

double Cylinder::get_radius() const {
	return radius_;
}

double Cylinder::get_height() const {
	return height_;
}

double Cylinder::area() const 
{
	return 2*M_PI * radius_ * (radius_ + height_);
}

double Cylinder::volume() const 
{
	return M_PI * radius_ * radius_ * height_;
}

std::ostream& Cylinder::print(std::ostream& os) const {
	os << "Cylinder: " << std::endl
		<< "position_:"<< position_.x << " " << position_.y << " " << position_.z
		<< " radius_:"<< radius_  
		<< " height_:"<< height_ << " ";
	Shape::print(os);

	return os;
}

bool Cylinder::intersect(Ray const& ray, float& distance, glm::vec3& intersection_point, glm::vec3& normal_at_intersection)
{
	//TO DO
	return false;
}


void Cylinder::translate(glm::vec3 const& offset)
{
	position_ += offset;
}

/*
void Cone::rotate(float angle, glm::vec3 vector)
{
	float radiant = angle * M_PI / 180;

	minimum_ = glm::rotate(minimum_, radiant, vector);
	maximum_ = glm::rotate(minimum_, radiant, vector);
}

void Box::scale(float value)
{
	maximum_ *= value;
}
*/
