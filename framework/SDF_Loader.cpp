#include "SDF_Loader.hpp"

#include <sstream>

SDF_Loader::SDF_Loader()
{}

SDF_Loader::~SDF_Loader()
{}


bool SDF_Loader::load_Scene(Scene& new_scene, std::string file)
{
	filestream_.open(file);
	std::string line;

	while (std::getline(filestream_, line))
	{
		std::istringstream iss_line(line);
		std::vector<std::string> words;
		std::string word;
		//std::string::size_type stringst;

		//std::cout << "string to parse: " << line << std::endl;

		while (std::getline(iss_line, word, ' '))
		{
			words.push_back(word);
		}

		auto it = words.begin();

		while (it != words.end())
		{
			if (*it == "#")
			{
				break;
			}

			else if (*it == "define")
			{
				++it;

				if (*it == "shape")
				{
					++it;
					
					if (*it == "box")
					{
						std::shared_ptr<Material> tmp_material_ptr;

						bool tmpb = new_scene.get_Material(words[10], tmp_material_ptr);

						if (new_scene.get_Material(words[10], tmp_material_ptr))
						{
							Box* tmp = new Box(
								words[3],
								glm::vec3(std::stof(words[4]), std::stof(words[5]), std::stof(words[6])),
								glm::vec3(std::stof(words[7]), std::stof(words[8]), std::stof(words[9])),
								tmp_material_ptr
								);

							new_scene.add_Box(tmp);

							std::cout << "ADDED: " << std::endl << *tmp << std::endl << std::endl;
							it = words.end();
							break;
						}
						else
						{
							std::cout << "Box: " << *++it << " konnte nicht hinzugefügt werden!" << std::endl
								<< "Material nicht gefunden!" << std::endl;
							it = words.end();
							break;
						}

					}
					else if (*it == "sphere")
					{
						std::shared_ptr<Material> tmp_material_ptr;
						if (new_scene.get_Material(words[8], tmp_material_ptr))
						{
							Sphere* tmp = new Sphere(
								words[3],
								glm::vec3(std::stof(words[4]), std::stof(words[5]), std::stof(words[6])), 
								std::stof(words[7]),
								tmp_material_ptr
								);

							new_scene.add_Sphere(tmp);

							std::cout << "ADDED: " << std::endl << *tmp << std::endl << std::endl;
							it = words.end();
							break;
						}
						else
						{
							std::cout << "Sphere: " << *++it << " konnte nicht hinzugefügt werden!" << std::endl
								<< "Material nicht gefunden!" << std::endl;
							it = words.end();
							break;
						}
					}
					else if (*it == "composite")
					{
						Composite tmp;

						tmp.composite_name_ = *++it;
						++it;

						while (it != words.end())
						{
							std::shared_ptr<Shape*> tmp_shape_ptr;

							if (new_scene.get_Shape(*it, tmp_shape_ptr))
							{
								tmp.composite_shapes_.push_back(tmp_shape_ptr);
							}
							else
							{
								std::cout << "Objekt: " << *it << " nicht zum Composite " << tmp.composite_name_ << " hinzugefuegt!" << std::endl
									<< *it << " nicht in den vorhanden Objekten gefunden!" << std::endl;
							}

							++it;
						}

						if (tmp.composite_shapes_.size() != 0)
						{
							new_scene.add_Composites(tmp);	
							std::cout << "ADDED: " << tmp << std::endl;
							it = words.end();
							break;
						}
					}
				}

				else if (*it == "material")
				{
					Material tmp;

					tmp.name = words[2];
					tmp.ka = Color(std::stof(words[3]), std::stof(words[4]), std::stof(words[5]));
					tmp.kd = Color(std::stof(words[6]), std::stof(words[7]), std::stof(words[8]));
					tmp.ks = Color(std::stof(words[9]), std::stof(words[10]), std::stof(words[11]));
					tmp.m = std::stof(words[12]);

					new_scene.add_Material(tmp);

					std::cout << "ADDED: " << std::endl << tmp << std::endl;

					it = words.end();	
					break;
				}

				else if (*it == "light")	
				{
					Light tmp;
					++it;

					if (*it == "ambient")
					{
						tmp.light_type = 0;
					}
					else
					{
						tmp.light_type = 1;
					}

					tmp.name = words[3];
					if (tmp.light_type == 1)
					{
						tmp.position = glm::vec3(std::stof(words[4]), std::stof(words[5]), std::stof(words[6]));
						tmp.ld = glm::vec3(std::stof(words[7]), std::stof(words[8]), std::stof(words[9]));
					}
					else
					{
						tmp.la = glm::vec3(std::stof(words[4]), std::stof(words[5]), std::stof(words[6]));
					}

					new_scene.add_Light(tmp);

					std::cout << "ADDED: " << tmp << std::endl;

					it = words.end();
					break;
				}

				else if (*it == "camera")
				{
					Camera tmp;
					tmp.name = words[2];
					tmp.fov_x = std::stof(words[3]);
					
					if (words.size() > 4)
					{
						tmp.position = glm::vec3(std::stof(words[4]), std::stof(words[5]), std::stof(words[6]));
						tmp.direction = glm::vec3(std::stof(words[7]), std::stof(words[8]), std::stof(words[9]));
						tmp.up = glm::vec3(std::stof(words[10]), std::stof(words[11]), std::stof(words[12]));

					}
					else
					{
						tmp.position = glm::vec3(0, 0, 0);
						tmp.direction = glm::vec3(0, 0, -1);
						tmp.up = glm::vec3(0, 1, 0);
					}
					new_scene.set_Camera(tmp);

					std::cout << "ADDED: " << tmp << std::endl;
					
					it = words.end();
					break;
				}

			}

			else if (*it == "transform")
			{
				++it;

				//std::shared_ptr<Box> tmp_box_ptr;
				//std::shared_ptr<Sphere> tmp_sphere_ptr;

				std::shared_ptr<Shape*> tmp_shape_ptr;

				if (*it == new_scene.get_Camera().name)
				{
					++it;
					Camera tmp = new_scene.get_Camera();

					if (*it == "rotate")
					{
						float radiant = std::stof(words[3]); //* M_PI / 180;
						tmp.position = glm::rotate(tmp.position, radiant, glm::vec3(std::stof(words[4]), std::stof(words[5]), std::stof(words[6])));
						tmp.direction = glm::rotate(tmp.direction, radiant, glm::vec3(std::stof(words[4]), std::stof(words[5]), std::stof(words[6])));

						std::cout << "TRANSFORMED: " << *it << " " << tmp.name << std::endl << std::endl;
					}
					else if (*it == "translate")
					{
						tmp.position += glm::vec3(std::stof(words[3]), std::stof(words[4]), std::stof(words[5]));
						//tmp.direction += glm::vec3(std::stof(words[3]), std::stof(words[4]), std::stof(words[5]));

						std::cout << "TRANSFORMED: " << *it << " " << tmp.name << std::endl << std::endl;
					}
					new_scene.set_Camera(tmp);
				}
				else if (new_scene.get_Shape(*it, tmp_shape_ptr))
				{
					++it;
					if (*it == "rotate")
					{
						(*tmp_shape_ptr)->rotate(std::stof(words[3]), glm::vec3(std::stof(words[4]), std::stof(words[5]), std::stof(words[6])));
						std::cout << "TRANSFORMED: " << *it << " " << (*tmp_shape_ptr)->get_name() << std::endl << std::endl;
					}
					else if (*it == "scale")
					{
						(*tmp_shape_ptr)->scale(std::stof(words[3]));						
						std::cout << "TRANSFORMED: " << *it << " " << (*tmp_shape_ptr)->get_name() << std::endl << std::endl;
					}
					else if (*it == "translate")
					{
						(*tmp_shape_ptr)->translate(glm::vec3(std::stof(words[3]), std::stof(words[4]), std::stof(words[5])));
						std::cout << "TRANSFORMED: " << *it << " " << (*tmp_shape_ptr)->get_name() << std::endl << std::endl;
					}
				}
				
				else if (new_scene.is_Composite(*it))
				{
					Composite tmp_comp = new_scene.get_Composite(*it);
					std::cout << "Transformation of the Composite " << tmp_comp.composite_name_ << ":" << std::endl;
					for (auto& tmp_shape_ptr : tmp_comp.composite_shapes_)					
					{
						++it;

						if (*it == "rotate")
						{
							(*tmp_shape_ptr)->rotate(std::stof(words[3]), glm::vec3(std::stof(words[4]), std::stof(words[5]), std::stof(words[6])));
							std::cout << " TRANSFORMED: " << *it << " " << (*tmp_shape_ptr)->get_name() << std::endl << std::endl;
						}
						else if (*it == "scale")
						{
							(*tmp_shape_ptr)->scale(std::stof(words[3]));						
							std::cout << " TRANSFORMED: " << *it << " " << (*tmp_shape_ptr)->get_name() << std::endl << std::endl;
						}
						else if (*it == "translate")
						{
							(*tmp_shape_ptr)->translate(glm::vec3(std::stof(words[3]), std::stof(words[4]), std::stof(words[5])));
							std::cout << " TRANSFORMED: " << *it << " " << (*tmp_shape_ptr)->get_name() << std::endl << std::endl;
						}
						--it;
					}
				}
				
				//else if (new_scene.get_Shape(*it, tmp_shape_ptr))
				//{
				//	++it;
				//	if (*it == "rotate")
				//	{
				//		(*tmp_shape_ptr)->rotate(std::stof(*++it), glm::vec3(std::stof(*++it), std::stof(*++it), std::stof(*++it)));
				//	}
				//	else if (*it == "scale")
				//	{
				//		(*tmp_shape_ptr)->scale(std::stof(*++it));
				//	}
				//	else if (*it == "translate")
				//	{
				//		(*tmp_shape_ptr)->translate(glm::vec3(std::stof(*++it), std::stof(*++it), std::stof(*++it)));
				//	}
				//}
			}
			else if (*it == "render")
			{
				++it;
				Renderer_Info tmp_renderer;

				if (*it == new_scene.get_Camera().name)
				{
					tmp_renderer.camera = std::make_shared<Camera>(new_scene.get_Camera());
					++it;
					tmp_renderer.filename = *++it;
					tmp_renderer.width = std::stoi(*++it);
					tmp_renderer.heigth = std::stoi(*++it);

					new_scene.set_Renderer(tmp_renderer);
				}
				else
				{
					std::cout << "FAILED to load render informations: Camera not found!" << std::endl;
					return false;
				}
			}


			it = words.end();
		}

	}

	//Checks
	if (new_scene.get_AllShapes().size() == 0)
	{
		std::cout << "FAILED to load scene: No Objects in scene!" << std::endl;
		return false;
	}

	return true;
	

}
