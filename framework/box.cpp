#include "box.hpp"


Box::Box():
	minimum_(glm::vec3(0.0,0.0,0.0)),
	maximum_(glm::vec3(0.0,0.0,0.0)),
	Shape("new_box") 
{

}

Box::Box(std::string const& name, glm::vec3 const& min, glm::vec3 const& max, std::shared_ptr<Material> const& material) :
minimum_(min),
maximum_(max),
Shape(name, material)
{}

Box::~Box()
{
	//std::cout << "Destruktor von Box" << std::endl;
}

glm::vec3 Box::get_min() const {	
	return minimum_;
}

glm::vec3 Box::get_max() const {
	return maximum_;
}

double Box::area() const 
{
	double length = maximum_.x - minimum_.x;
	double heigth = maximum_.y - minimum_.y;
	double width = maximum_.z - minimum_.z;
	return 2*(heigth*width) + 2*(heigth*length) + 2*(width*length);
}

double Box::volume() const 
{
	double length = maximum_.x - minimum_.x;
	double heigth = maximum_.y - minimum_.y;
	double width = maximum_.z - minimum_.z;
	return length * heigth * width;
}

std::ostream& Box::print(std::ostream& os) const {
	
	Shape::print(os);
	os << "Typ: Box " << std::endl
		<< "minimum_:" << minimum_.x << " " << minimum_.y << " " << minimum_.z << std::endl
		<< "maximum_:" << maximum_.x << " " << maximum_.y << " " << maximum_.z << std::endl;
	

	return os;
}

bool Box::intersect(Ray const& ray, float& distance, glm::vec3& intersection_point, glm::vec3& normal_at_intersection)
{
	Ray r_modified = ray * this->get_transformation_matrix();

	float tmin, tmax, tymin, tymax, tzmin, tzmax;

	tmin = (minimum_.x - r_modified.origin.x) / glm::normalize(r_modified.direction).x;
	tmax = (maximum_.x - r_modified.origin.x) / glm::normalize(r_modified.direction).x;

	if (tmin > tmax) std::swap(tmin, tmax);

	tymin = (minimum_.y - r_modified.origin.y) / glm::normalize(r_modified.direction).y;
	tymax = (maximum_.y - r_modified.origin.y) / glm::normalize(r_modified.direction).y;

	if (tymin > tymax) std::swap(tymin, tymax);
	if ((tmin > tymax) || (tymin > tmax))
		return false;

	if (tymin > tmin) tmin = tymin;
	if (tymax < tmax) tmax = tymax;

	tzmin = (minimum_.z - r_modified.origin.z) / glm::normalize(r_modified.direction).z;
	tzmax = (maximum_.z - r_modified.origin.z) / glm::normalize(r_modified.direction).z;

	if (tzmin > tzmax) std::swap(tzmin, tzmax);
	if ((tmin > tzmax) || (tzmin > tmax))
		return false;

	if (tzmin > tmin) tmin = tzmin;
	if (tzmax < tmax) tmax = tzmax;

	//std::cout << "min: " << tmin << " max: " << tmax << std::endl;
	float eps = 0.1f;
	if (tmin > tmax)
	{
		distance = tmin;

	}
	else
	{
		distance = tmax;
	}

	glm::vec4 intersection_point4 = glm::vec4( (r_modified.origin + distance * glm::normalize(r_modified.direction) ), 1.0);
	//glm::vec4 normal_at_intersection4 = glm::vec4(get_normal_at(intersection_point4), 0.0);

	intersection_point = glm::vec3(intersection_point4 * glm::inverse(this->get_transformation_matrix()));
	normal_at_intersection =  this->get_normal_at(intersection_point4);
	//normal_at_intersection =  get_normal_at(intersection_point4);
	return true;








	//glm::vec3 norm_ray_dir = glm::normalize(ray.direction);

	//float tmp_distance = INFINITY;

	//int hits = 0;


	//unterscheid von welcher Richtung der Ray kommt (vorn/hinten, links/rechts, oben/unten
	//KOOS
	//	y		oben (Ebene parallel zu x-z-Ebene)
	//	|
	//	-x		links (Ebene parallel zu y-z-Ebene)
	// /
	// z		vorn (Ebene parallel zu x-y-Ebene)

	//------------------------------------------------------------

	//////vorn
	////if (ray.origin.z < minimum_.z)
	////{
	//	if (geometry::intersectionRayPlane(
	//		ray.origin,
	//		norm_ray_dir,
	//		minimum_,
	//		glm::vec3(maximum_.x, maximum_.y, minimum_.z),
	//		tmp_distance))
	//	{
	//		std::cout << "vorn" << std::endl;
	//		if (tmp_distance < distance)
	//		{
	//			distance = tmp_distance;
	//			hits++;
	//		}
	//	}
	////}
	//////hinten
	////else if (ray.origin.z > maximum_.z)
	////{
	//	if (geometry::intersectionRayPlane(
	//		ray.origin,
	//		norm_ray_dir,
	//		glm::vec3(maximum_.x, minimum_.y, maximum_.z),
	//		glm::vec3(minimum_.x, maximum_.y, maximum_.z),
	//		tmp_distance))
	//	{
	//		std::cout << "hinten" << std::endl;

	//		if (tmp_distance < distance)
	//		{
	//			distance = tmp_distance;
	//			hits++;
	//		}
	//	}
	////}


	//////links
	////if (ray.origin.x < minimum_.x)
	////{
	//	if (geometry::intersectionRayPlane(
	//		ray.origin,
	//		norm_ray_dir,
	//		glm::vec3(minimum_.x, minimum_.y, maximum_.z),
	//		glm::vec3(minimum_.x, maximum_.y, minimum_.z),
	//		tmp_distance))
	//	{
	//		std::cout << "links" << std::endl;

	//		if (tmp_distance < distance)
	//		{
	//			distance = tmp_distance;
	//			hits++;
	//		}
	//	}
	////}
	//////rechts
	////else if (ray.origin.x > maximum_.x)
	////{
	//	if (geometry::intersectionRayPlane(
	//		ray.origin,
	//		norm_ray_dir,
	//		glm::vec3(maximum_.x, minimum_.y, minimum_.z),
	//		maximum_,
	//		tmp_distance))
	//	{
	//		if (tmp_distance < distance)
	//		{
	//			distance = tmp_distance;
	//			hits++;
	//		}
	//	}
	////}

	//////unten
	////if (ray.origin.y < minimum_.y)
	////{
	//	if (geometry::intersectionRayPlane(
	//		ray.origin,
	//		norm_ray_dir,
	//		glm::vec3(minimum_.x, minimum_.y, maximum_.z),
	//		glm::vec3(maximum_.x, minimum_.y, minimum_.z),
	//		tmp_distance))
	//	{
	//		if (tmp_distance < distance)
	//		{
	//			distance = tmp_distance;
	//			hits++;
	//		}
	//	}
	////}
	//////oben
	////else if (ray.origin.y > maximum_.y)
	////{
	//	if (geometry::intersectionRayPlane(
	//		ray.origin,
	//		norm_ray_dir,
	//		glm::vec3(minimum_.x, maximum_.y, minimum_.z),
	//		maximum_,
	//		tmp_distance))
	//	{
	//		if (tmp_distance < distance)
	//		{
	//			distance = tmp_distance;
	//			hits++;
	//		}
	//	}
	////}

	//------------------------------------------------------------

	//Ebene der Box Bsp.
	//   _____
	//	/___/|
	//  |	|/
	//  -----
	//  Ansicht von vorn

	//  _____
    //  |    |
	//  ------
	// Ebene von vorn
	// Punkte:	unten rechts Ursprung (originPoint)
	//			unten links zweiter Punkt (seconsPoint)
	//			oben rechts dritter Punkt (thirdPoint)

	// bei der Funktion ohne GLM entfällt originPoint (also nur die beiden gegenüberliegenden Punkte werden verwendet)



	//////vorn
	////if (ray.origin.z < minimum_.z)
	////{
	//	if (geometry::intersectionRayPlaneGLM(
	//		ray.origin,
	//		norm_ray_dir,
	//		glm::vec3(minimum_.x, maximum_.y, maximum_.z),
	//		minimum_,
	//		glm::vec3(maximum_.x, maximum_.y, minimum_.z),
	//		tmp_distance))
	//	{
	//		std::cout << "vorn" << std::endl;
	//		if (tmp_distance < distance)
	//		{
	//			distance = tmp_distance;
	//			hits++;
	//		}
	//	}
	////}
	//////hinten
	////else if (ray.origin.z > maximum_.z)
	////{
	//	if (geometry::intersectionRayPlaneGLM(
	//		ray.origin,
	//		norm_ray_dir,
	//		glm::vec3(minimum_.x, minimum_.y, maximum_.z),
	//		glm::vec3(maximum_.x, minimum_.y, maximum_.z),
	//		glm::vec3(minimum_.x, maximum_.y, maximum_.z),
	//		tmp_distance))
	//	{
	//		//std::cout << "hinten" << std::endl;
	//		if (tmp_distance < distance)
	//		{
	//			distance = tmp_distance;
	//			hits++;
	//		}
	//	}
	////}


	//////links
	////if (ray.origin.x < minimum_.x)
	////{
	//	if (geometry::intersectionRayPlaneGLM(
	//		ray.origin,
	//		norm_ray_dir,
	//		minimum_,
	//		glm::vec3(minimum_.x, minimum_.y, maximum_.z),
	//		glm::vec3(minimum_.x, maximum_.y, minimum_.z),
	//		tmp_distance))
	//	{
	//		//std::cout << "links" << std::endl;

	//		if (tmp_distance < distance)
	//		{
	//			distance = tmp_distance;
	//			hits++;
	//		}
	//	}
	////}
	//////rechts
	////else if (ray.origin.x > maximum_.x)
	////{
	//	if (geometry::intersectionRayPlaneGLM(
	//		ray.origin,
	//		norm_ray_dir,
	//		glm::vec3(maximum_.x, minimum_.y, maximum_.z),
	//		glm::vec3(maximum_.x, minimum_.y, minimum_.z),
	//		maximum_,
	//		tmp_distance))
	//	{
	//		//std::cout << "rechts" << std::endl;

	//		if (tmp_distance < distance)
	//		{
	//			distance = tmp_distance;
	//			hits++;
	//		}
	//	}
	////}

	//////unten
	////if (ray.origin.y < minimum_.y)
	////{
	//	if (geometry::intersectionRayPlaneGLM(
	//		ray.origin,
	//		norm_ray_dir,
	//		glm::vec3(maximum_.x, minimum_.y, maximum_.z),
	//		glm::vec3(minimum_.x, minimum_.y, maximum_.z),
	//		glm::vec3(maximum_.x, minimum_.y, minimum_.z),
	//		tmp_distance))
	//	{
	//		//std::cout << "unten" << std::endl;

	//		if (tmp_distance < distance)
	//		{
	//			distance = tmp_distance;
	//			hits++;
	//		}
	//	}
	////}
	//////oben
	////else if (ray.origin.y > maximum_.y)
	////{
	//	if (geometry::intersectionRayPlaneGLM(
	//		ray.origin,
	//		norm_ray_dir,
	//		glm::vec3(maximum_.x, maximum_.y, minimum_.z),
	//		glm::vec3(minimum_.x, maximum_.y, minimum_.z),
	//		maximum_,
	//		tmp_distance))
	//	{
	//		//std::cout << "oben" << std::endl;

	//		if (tmp_distance < distance)
	//		{
	//			distance = tmp_distance;
	//			hits++;
	//		}
	//	}
	////}



	//if (hits != 0)
	//{
	//	return true;
	//}

	
	
	//return false;


}


void Box::translate(glm::vec3 const& offset)
{
	//minimum_ += offset;
	//maximum_ += offset;

	//glm::mat4 tmp_TM = this->get_transformation_matrix();
	//glm::mat4 tmp_TM_inv = this->get_transformation_matrix_inv();

	//glm::vec4 t_vec = tmp_TM[3];
	//t_vec += glm::vec4(offset, 0.0f);

	//tmp_TM[3] = t_vec;

	//t_vec = tmp_TM_inv[3];
	//t_vec += glm::vec4(-offset, 0.0f);

	//tmp_TM_inv[3] = t_vec;


	glm::mat4 T = glm::mat4(
		1.0f, 0.0f, 0.0f, offset.x,
		0.0f, 1.0f, 0.0f, offset.y,
		0.0f, 0.0f, 1.0f, offset.z,
		0.0f, 0.0f, 0.0f, 1.0f);

	glm::mat4 T_inv = glm::mat4(
		1.0f, 0.0f, 0.0f, -offset.x,
		0.0f, 1.0f, 0.0f, -offset.y,
		0.0f, 0.0f, 1.0f, -offset.z,
		0.0f, 0.0f, 0.0f, 1.0f);

	Shape::set_transformation_matrix(T, T_inv);

}

void Box::rotate(float const& angle, glm::vec3 const& vector)
{
	float rad = angle * M_PI / 180;

	//minimum_ = glm::rotate(minimum_, radiant, vector);
	//maximum_ = glm::rotate(minimum_, radiant, vector);

	//glm::mat4 tmp_TM = this->get_transformation_matrix();
	//glm::mat4 tmp_TM_inv = this->get_transformation_matrix_inv();

	glm::mat4 R(1.0);

	if (vector.x != 1.0)
	{
		R = glm::mat4(
			1.0, 0.0,		0.0,		0.0,
			0.0, cos(rad), -sin(rad),	0.0,
			0.0, sin(rad), cos(rad),	0.0,
			0.0, 0.0,		0.0,		1.0);
	}
	else if (vector.y != 0.0)
	{
		R = glm::mat4(
			cos(angle),		0.0, sin(angle), 0.0,
			0.0,			0.0, 0.0,		 0.0,
			-sin(angle),	0.0, cos(rad),	 0.0,
			0.0,			0.0, 0.0,		 1.0);
	}
	else if (vector.z != 0.0)
	{
		R = glm::mat4(
			cos(angle), -sin(angle),	0.0,	0.0,
			sin(angle), cos(rad),		0.0,	0.0,
			0.0,		0.0,			0.0,	0.0,
			0.0,		0.0,			0.0,	1.0);
	}

	glm::mat4 R_inv = glm::inverse(R);

	Shape::set_transformation_matrix(R, R_inv);

}

void Box::scale(float const& value)
{
	//maximum_ *= value;

	//glm::mat4 tmp_TM = this->get_transformation_matrix();
	//glm::mat4 tmp_TM_inv = this->get_transformation_matrix_inv();

	//for (int i = 0; i < 4; i++)
	//{
	//	tmp_TM[i][i] *= value;
	//	tmp_TM_inv[i][i] *= 1 / value;
	//}

	glm::mat4 S = glm::mat4(
		value, 0.0f, 0.0f, 0.0f,
		0.0f, value, 0.0f, 0.0f,
		0.0f, 0.0f, value, 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f);

	glm::mat4 S_inv = glm::mat4(
		1/value, 0.0f, 0.0f, 0.0f,
		0.0f, 1/value, 0.0f, 0.0f,
		0.0f, 0.0f, 1/value, 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f);

	Shape::set_transformation_matrix(S, S_inv);
}


glm::vec3 Box::get_normal_at(glm::vec4& point)
{
	float eps = 0.001f;

	glm::vec4 tmp_min = glm::vec4(minimum_, 1.0) * Shape::get_transformation_matrix();
	glm::vec4 tmp_max = glm::vec4(maximum_, 1.0) * Shape::get_transformation_matrix();
	
	if (geometry::equal(point.z, tmp_min.z))
	{
		//point.z += eps;
		return glm::vec3(0.0, 0.0, 1.0);
	}
	else if (geometry::equal(point.z, tmp_max.z))
	{
		//point.z -= eps;
		return glm::vec3(0.0, 0.0, -1.0);
	}
	else if (geometry::equal(point.y, tmp_min.y))
	{
		//point.y -= eps;
		return glm::vec3(0.0, -1.0, 0.0);
	}
	else if (geometry::equal(point.y, tmp_max.y))
	{
		//point.y += eps;
		return glm::vec3(0.0, 1.0, 0.0);
	}
	else if (geometry::equal(point.x, tmp_min.x))
	{
		//point.x -= eps;
		return glm::vec3(1.0, 0.0, 0.0);
	}
	else if (geometry::equal(point.x, tmp_max.x))
	{
		//point.x += eps;
		return glm::vec3(-1.0, 0.0, 0.0);
	}
}