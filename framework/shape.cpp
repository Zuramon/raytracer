#include "shape.hpp"


Shape::Shape():
name_("newShape"),
material_()
{
	world_transformation_ = glm::mat4(1.0);
	world_transformation_inv_ = glm::mat4(1.0);
}

Shape::Shape(std::string name) :
name_(name),
material_()
{
	world_transformation_ = glm::mat4(1.0);
	world_transformation_inv_ = glm::mat4(1.0);
}

Shape::Shape(std::string const& name, std::shared_ptr<Material> const& material) :
name_(name),
material_(material)
{
	world_transformation_ = glm::mat4(1.0);
	world_transformation_inv_ = glm::mat4(1.0);
}

Shape::~Shape()
{}

std::string Shape::get_name() const {
	return name_;
}


std::shared_ptr<Material> Shape::get_material() const
{
	return material_;
}


std::ostream& Shape::print(std::ostream& os) const 
{
	/*float r = material_->ka.r;
	float g = material_->ka.g;
	float b = material_->ka.b;*/
	os << "Shape:" << name_
		<< std::endl << "Material: " << material_->name << std::endl;


	return os;
}

void Shape::set_transformation_matrix(glm::mat4 transformation, glm::mat4 inv_transformation)
{
	world_transformation_ = transformation;
	world_transformation_inv_ = transformation;
}

glm::mat4 Shape::get_transformation_matrix() const
{
	return world_transformation_;
}

glm::mat4 Shape::get_transformation_matrix_inv() const
{
	return world_transformation_inv_;
}