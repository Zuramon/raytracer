#include "sphere.hpp"

Sphere::Sphere():
	Shape("new_sphere"),
	center_(glm::vec3(0.0,0.0,0.0)),
	radius_(1.0)
{}
	

Sphere::Sphere(std::string const& name, glm::vec3 const& center, double const& radius, std::shared_ptr<Material> const& material) :
	center_(center),
	radius_(radius),
	Shape(name, material) 
{}

Sphere::~Sphere()
{}

glm::vec3 Sphere::get_middle() const {
	return center_;
}

double Sphere::get_radius() const {
	return radius_;
}

double Sphere::area() const {
	return 4 * M_PI * pow(radius_, 2);
}

double Sphere::volume() const {
	return 1.33333 * M_PI * pow(radius_, 3);
}

std::ostream& Sphere::print(std::ostream& os) const 
{
	os << "Sphere:" << std::endl
		<< "middle:" << center_.x << " " << center_.y << " " << center_.z
		<< " radius:" << radius_ << " ";
	Shape::print(os);

	return os;
}

bool Sphere::intersect(Ray const& ray, float& distance, glm::vec3& intersection_point, glm::vec3& normal_at_intersection) {
	
	Ray r_transformed = ray * /*glm::inverse*/(this->get_transformation_matrix_inv());

	auto result = glm::intersectRaySphere(
		r_transformed.origin, 
		glm::normalize(r_transformed.direction),
		center_,
		radius_ * radius_,
		distance);
	
	glm::vec4 intersection_point4 = glm::vec4((r_transformed.origin + distance * glm::normalize(r_transformed.direction)), 1.0);

	intersection_point = glm::vec3(intersection_point4 * /*glm::inverse*/(this->get_transformation_matrix()));
	//intersection_point = ray.origin + distance * glm::normalize(ray.direction);

	normal_at_intersection = get_normal_at(intersection_point4);
	//normal_at_intersection = get_normal_at(glm::vec4(intersection_point, 1.0));

	return result;
}


void Sphere::translate(glm::vec3 const& offset)
{
	//center_ += offset;

	glm::mat4 T = glm::mat4(
		1.0f, 0.0f, 0.0f, offset.x,
		0.0f, 1.0f, 0.0f, offset.y,
		0.0f, 0.0f, 1.0f, offset.z,
		0.0f, 0.0f, 0.0f, 1.0f);

	glm::mat4 T_inv = glm::mat4(
		1.0f, 0.0f, 0.0f, -offset.x,
		0.0f, 1.0f, 0.0f, -offset.y,
		0.0f, 0.0f, 1.0f, -offset.z,
		0.0f, 0.0f, 0.0f, 1.0f);

	Shape::set_transformation_matrix(T, T_inv);
}

void Sphere::rotate(float const& angle, glm::vec3 const& vector)
{
	float rad = angle * M_PI / 180;

	//center_ = glm::rotate(center_, radiant, vector);

	glm::mat4 R(1.0);

	if (vector.x != 1.0)
	{
		R = glm::mat4(
			1.0, 0.0, 0.0, 0.0,
			0.0, cos(rad), -sin(rad), 0.0,
			0.0, sin(rad), cos(rad), 0.0,
			0.0, 0.0, 0.0, 1.0);
	}
	else if (vector.y != 0.0)
	{
		R = glm::mat4(
			cos(angle), 0.0, sin(angle), 0.0,
			0.0, 0.0, 0.0, 0.0,
			-sin(angle), 0.0, cos(rad), 0.0,
			0.0, 0.0, 0.0, 1.0);
	}
	else if (vector.z != 0.0)
	{
		R = glm::mat4(
			cos(angle), -sin(angle), 0.0, 0.0,
			sin(angle), cos(rad), 0.0, 0.0,
			0.0, 0.0, 0.0, 0.0,
			0.0, 0.0, 0.0, 1.0);
	}

	glm::mat4 R_inv = glm::inverse(R);

	Shape::set_transformation_matrix(R, R_inv);
}

void Sphere::scale(float const& value)
{
	//radius_ *= value;

	glm::mat4 S = glm::mat4(
		value, 0.0f, 0.0f, 0.0f,
		0.0f, value, 0.0f, 0.0f,
		0.0f, 0.0f, value, 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f);

	glm::mat4 S_inv = glm::mat4(
		1 / value, 0.0f, 0.0f, 0.0f,
		0.0f, 1 / value, 0.0f, 0.0f,
		0.0f, 0.0f, 1 / value, 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f);

	Shape::set_transformation_matrix(S, S_inv);
}


//std::ostream& operator<<(std::ostream& os, const Sphere& s)
//{
//	std::ostream::sentry cerberus(os);
//	if (cerberus)
//		s.print(os);
//	return os;
//}

glm::vec3 Sphere::get_normal_at(glm::vec4& point)
{
	//glm::vec3 tmp_point(point.x, point.y, point.z);
	glm::vec4 tmp_center = glm::vec4(center_, 1.0f) * this->get_transformation_matrix();

	glm::vec4 tmp_normal = tmp_center - point;

	tmp_normal = tmp_normal * glm::transpose(/*glm::inverse*/(this->get_transformation_matrix_inv()));

	return glm::vec3(tmp_normal.x, tmp_normal.y, tmp_normal.z);
}