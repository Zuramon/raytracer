#ifndef BUW_SHAPE_HPP
#define BUW_SHAPE_HPP

#define _USE_MATH_DEFINES
#include <cmath>

#include <glm/glm.hpp>
#include <glm/gtx/intersect.hpp>
#include <glm/vec3.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <string>
#include <iostream>
#include <memory>


#include "ray.hpp"
#include "Material.hpp"
#include "Geometry.hpp"

class Shape{

public:

	Shape();

	Shape(std::string name);

	Shape(std::string const& name, std::shared_ptr<Material> const& material);

	virtual ~Shape();

	virtual double area() const = 0;

	virtual double volume() const = 0;

	std::string get_name() const;

	std::shared_ptr<Material> get_material() const;

	virtual std::ostream& print(std::ostream& os) const;

	virtual bool intersect(Ray const& ray, float& distance, glm::vec3& intersection_point, glm::vec3& normal_at_intersection) = 0;

	virtual void translate(glm::vec3 const& offset) = 0;

	virtual void rotate(float const& angle, glm::vec3 const& vector) = 0;

	virtual void scale(float const& value) = 0;

	virtual glm::vec3 get_normal_at(glm::vec4& point) = 0;

	void set_transformation_matrix(glm::mat4 transformation, glm::mat4 inv_transformation);

	glm::mat4 get_transformation_matrix() const;
	glm::mat4 get_transformation_matrix_inv() const;

private:

	const std::string name_;
	std::shared_ptr<Material> material_;

	glm::mat4 world_transformation_;
	glm::mat4 world_transformation_inv_;
};

inline std::ostream& operator<<(std::ostream& os, Shape const& s)
{
	s.print(os);
	return os;
}




#endif //#define BUW_SHAPE_HPP