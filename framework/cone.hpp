#ifndef BUW_CONE_HPP
#define BUW_CONE_HPP

#include "shape.hpp"

class Cone: public Shape{

public:

	Cone();

	Cone(std::string const& name, glm::vec3 const& position, double const& radius, double const& height, std::shared_ptr<Material> const& material);

	~Cone();

	glm::vec3 get_position() const;

	double get_radius() const;

	double get_height() const;

	double area() const override;
	
	double volume() const override;

	std::ostream& print(std::ostream& os) const;

	bool intersect(Ray const& ray, float& distance, glm::vec3& intersection_point, glm::vec3& normal_at_intersection) override;

	void translate(glm::vec3 const& offset) override;

	void rotate(float const& angle, glm::vec3 const& vector) override;

	void scale(float const& value) override;


private:

	glm::vec3 position_;
	double radius_;
	double height_;
	
};

#endif //#define BUW_CONE_HPP
