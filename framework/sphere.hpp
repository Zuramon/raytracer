#ifndef BUW_SPHERE_HPP
#define BUW_SPHERE_HPP

#include "shape.hpp"

class Sphere: public Shape{

public:

	Sphere();

	//Sphere(glm::vec3 const& p, double const& r);

	Sphere(std::string const& name, glm::vec3 const& center, double const& radius, std::shared_ptr<Material> const& material);

	~Sphere();

	glm::vec3 get_middle() const;

	double get_radius() const;

	double area() const override;

	double volume() const override;

	std::ostream& print(std::ostream& os) const;

	bool intersect(Ray const& ray, float& distance, glm::vec3& intersection_point, glm::vec3& normal_at_intersection) override;


	void translate(glm::vec3 const& offset) override;

	void rotate(float const& angle, glm::vec3 const& vector) override;

	void scale(float const& value) override;

	glm::vec3 get_normal_at(glm::vec4& point) override;


private:

	glm::vec3 center_;
	double radius_;
};

inline std::ostream& operator<<(std::ostream& os, Sphere const& s)
{
	s.print(os);
	return os;
}

#endif //#define BUW_SPHERE_HPP