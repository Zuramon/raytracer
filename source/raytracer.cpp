#include <thread>
#include <renderer.hpp>
#include <fensterchen.hpp>

#include "SDF_Loader.hpp"

int main(int argc, char* argv[])
{
  unsigned const width = 600;
  unsigned const height = 600;
  std::string const filename = "./checkerboard.ppm";
  std::string const sdf_file = "./default_scene.sdf";

  SDF_Loader sdf_load;
  Scene my_scene; 
  if (!sdf_load.load_Scene(my_scene, sdf_file))
  {
	  std::cout << "FAILED TO LOAD SCENE!" << std::endl;
  }
  else
  {

	  Renderer app(my_scene.get_renderer().width, my_scene.get_renderer().heigth, my_scene.get_renderer().filename, my_scene);

	  std::thread thr([&app]() { app.render(); });

	  Window win(glm::ivec2(my_scene.get_renderer().width, my_scene.get_renderer().heigth));

	  while (!win.shouldClose()) {
		  if (win.isKeyPressed(GLFW_KEY_ESCAPE)) {
			  win.stop();
		  }

		  glDrawPixels(my_scene.get_renderer().width, my_scene.get_renderer().heigth, GL_RGB, GL_FLOAT
			  , app.colorbuffer().data());

		  win.update();
	  }

	  thr.join();
  }

  return 0;
}
